<?php

namespace Drupal\formfactorykits\Kits\Field;

class ValueKit extends FieldKit {
    public static ?string $id = 'value';
    public static ?string $type = 'value';
}
