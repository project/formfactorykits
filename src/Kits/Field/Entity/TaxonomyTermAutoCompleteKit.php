<?php

namespace Drupal\formfactorykits\Kits\Field\Entity;

use Drupal\formfactorykits\Kits\Traits\ConfigFactoryTrait;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class TaxonomyTermAutoCompleteKit extends EntityAutoCompleteKit {
  use DescriptionTrait;
  use TitleTrait;
  use ConfigFactoryTrait;

  const VOCABULARY_KEY = 'vid';

  public static ?string $id = 'entity_autocomplete';
  public static ?string $targetType = 'taxonomy_term';

  public function getArray(): array
  {
    if ($this->has('title')) {
      return parent::getArray();
    }
    $this->setTitle($this->getTaxonomyName($this->get(self::VOCABULARY_KEY)));
    return parent::getArray();
  }

  public function setVocabulary(string $vid)
  {
    return $this->set(self::VOCABULARY_KEY, $vid);
  }

  private function getTaxonomyName(string $vid): string
  {
    $configName = sprintf('taxonomy.vocabulary.%s', $vid);
    $data = $this->getConfigData($configName);
    return array_key_exists('name', $data) ? $data['name'] : '';
  }

}
