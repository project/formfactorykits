<?php

namespace Drupal\formfactorykits\Kits\Field\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\formfactorykits\Kits\Field\Select\SelectKit;
use Drupal\formfactorykits\Kits\Traits\ConfigFactoryTrait;
use Drupal\formfactorykits\Kits\Traits\EntityTrait;
use Drupal\kits\Services\KitsInterface;
use Drupal\taxonomy\Entity\Term;

class TaxonomyTermSelectKit extends SelectKit {
    use ConfigFactoryTrait;
    use EntityTrait;
    use StringTranslationTrait;

    public const TRIM_LOADED_NAME_KEY = 'trim_loaded_name';

    public static ?string $id = 'term';

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::TRIM_LOADED_NAME_KEY, $context)) {
            $context[self::TRIM_LOADED_NAME_KEY] = TRUE;
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function loadTaxonomyVocabulary(string $vid): static
    {
        if (!$this->has('title')) {
            $name = $this->getTaxonomyName($vid);
            if (!$this->isMultiple() && $this->isTrimLoadedName()) {
                $name = rtrim($name, 's');
            }
            $this->setTitle($name);
        }
        foreach ($this->getTerms($vid) as $term) {
            $this->appendOption([$term->id() => $this->t($term->getName())]);
        }
        return $this;
    }

    private function getTaxonomyName(string $vid): string
    {
        $configName = sprintf('taxonomy.vocabulary.%s', $vid);
        $data = $this->getConfigData($configName);
        return array_key_exists('name', $data) ? $data['name'] : '';
    }

    /**
     * @return Term[]
     */
    private function getTermIds(string $vid): array
    {
        $result = $this->getEntityQuery('taxonomy_term')
            ->condition('vid', $vid)
            ->execute();
        return empty($result) ? [] : $result;
    }

    /**
     * @return EntityInterface[]|Term[]
     */
    private function getTerms(string $vid): array
    {
        $tids = $this->getTermIds($vid);
        return empty($tids) ? $tids : Term::loadMultiple($tids);
    }

    public function setMultiple(bool $isMultiple = TRUE): static
    {
        $this->setTrimLoadedName(FALSE);
        parent::setMultiple($isMultiple);
        return $this;
    }

    public function setTrimLoadedName(bool $trim = TRUE): static
    {
        return $this->setContext(self::TRIM_LOADED_NAME_KEY, $trim);
    }

    public function isTrimLoadedName()
    {
        return (bool)$this->getContext(self::TRIM_LOADED_NAME_KEY);
    }
}
