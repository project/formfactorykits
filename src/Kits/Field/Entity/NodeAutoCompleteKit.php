<?php

namespace Drupal\formfactorykits\Kits\Field\Entity;

use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class NodeAutoCompleteKit extends EntityAutoCompleteKit {
    use DescriptionTrait;
    use TitleTrait;

    public const TARGET_BUNDLES_KEY = 'target_bundles';

    public static ?string $id = 'node_autocomplete';
    public static ?string $targetType = 'node';
    public static ?string $title = 'Node';

    public function setTargetBundle(string $bundle): static
    {
        return $this->setTargetBundles([$bundle]);
    }

    public function setTargetBundles(array $bundles): static
    {
        return $this->setSelectionSetting(self::TARGET_BUNDLES_KEY, $bundles);
    }
}
