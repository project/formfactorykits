<?php

namespace Drupal\formfactorykits\Kits\Field\Entity;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\kits\Services\KitsInterface;
use Drupal\query\Common\Condition;
use Drupal\query\Common\Operator;

abstract class EntityAutoCompleteKit extends FieldKit {

    public const TAGS_KEY = 'tags';
    public const SELECTION_HANDLER_KEY = 'selection_handler';
    public const SELECTION_SETTINGS_KEY = 'selection_settings';
    public const TARGET_TYPE_KEY = 'target_type';

    public static ?string $type = 'entity_autocomplete';
    public static ?string $targetType = NULL;
    public static string $selectionHandler = 'default:entity_by_field';

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::TARGET_TYPE_KEY, $parameters)) {
            $parameters[self::TARGET_TYPE_KEY] = static::$targetType;
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function setTargetType(string $type): static
    {
        return $this->set(static::TARGET_TYPE_KEY, $type);
    }

    public function getSelectionHandler(): ?string
    {
        return $this->get(static::SELECTION_HANDLER_KEY);
    }

    public function setSelectionHandler(string $pluginID): static
    {
        return $this->set(static::SELECTION_HANDLER_KEY, $pluginID);
    }

    public function getFilter(string $key, ?string $default = NULL): string|array|null
    {
        $filters = $this->getSelectionSetting('filter', []);
        return $filters[$key] ?? $default;
    }

    public function setFilter(string $key, string|array $value): static
    {
        if (NULL !== static::$selectionHandler && !$this->getSelectionHandler()) {
            $this->setSelectionHandler(static::$selectionHandler);
        }
        $filters = $this->getSelectionSetting('filter', []);
        $filters[$key] = $value;
        return $this->setSelectionSetting('filter', $filters);
    }

    public function setCondition(Condition $condition): static
    {
        return $this->setFilter($condition->getKey(), $this->getSelectionSettingArray($condition));
    }

    public function getSelectionSettings(): array
    {
        return $this->get(self::SELECTION_SETTINGS_KEY, []);
    }

    public function setSelectionSettings(array $settings): static
    {
        return $this->set(self::SELECTION_SETTINGS_KEY, $settings);
    }

    public function getSelectionSetting(string $key, string|array|null $default = NULL): string|array|null
    {
        $settings = $this->getSelectionSettings();
        return $settings[$key] ?? $default;
    }

    public function setSelectionSetting(string $key, array|string|null $value): static
    {
        $settings = $this->getSelectionSettings();
        $settings[$key] = $value;
        return $this->setSelectionSettings($settings);
    }

    public function getSelectionSettingArray(Condition $condition): array
    {
        foreach ($condition->getRequirementGroups() as $group) {
            foreach ($group->getRequirements() as $expression) {
                $operator = $expression->getOperator();
                return match ($operator) {
                    Operator::TYPE_EQUIVALENT, Operator::TYPE_EQUALS => $expression->getValue(),
                    Operator::TYPE_NOT_EQUIVALENT, Operator::TYPE_NOT_EQUALS => [
                        'operator' => '!=',
                        'value' => $expression->getValue(),
                    ],
                    Operator::TYPE_LESS_THAN => [
                        'operator' => '<',
                        'value' => $expression->getValue(),
                    ],
                    Operator::TYPE_LESS_THAN_EQUAL_TO => [
                        'operator' => '<=',
                        'value' => $expression->getValue(),
                    ],
                    Operator::TYPE_GREATER_THAN => [
                        'operator' => '>',
                        'value' => $expression->getValue(),
                    ],
                    Operator::TYPE_GREATER_THAN_EQUAL_TO => [
                        'operator' => '>=',
                        'value' => $expression->getValue(),
                    ],
                    Operator::TYPE_IN => [
                        'operator' => 'IN',
                        'value' => $expression->getValues(),
                    ],
                    Operator::TYPE_NOT_IN => [
                        'operator' => 'NOT IN',
                        'value' => $expression->getValues(),
                    ],
                    default => throw new \DomainException(vsprintf('Unsupported operator: %s', [
                        $operator,
                    ])),
                };
            }
        }
        return [];
    }
}
