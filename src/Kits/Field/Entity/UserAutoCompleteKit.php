<?php

namespace Drupal\formfactorykits\Kits\Field\Entity;

use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class UserAutoCompleteKit extends EntityAutoCompleteKit {
    use DescriptionTrait;
    use TitleTrait;

    public static ?string $id = 'user_autocomplete';
    public static ?string $targetType = 'user';
    public static ?string $title = 'User';
}
