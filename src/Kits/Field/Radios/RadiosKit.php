<?php

namespace Drupal\formfactorykits\Kits\Field\Radios;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\OptionsTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class RadiosKit extends FieldKit {
    use DescriptionTrait;
    use OptionsTrait;
    use TitleTrait;
    use RequiredTrait;

    public const OPTIONS_KEY = 'options';

    public static ?string $id = 'radios';
    public static ?string $type = 'radios';

    public function getArray(): array
    {
        $artifact = parent::getArray();
        if (isset($this->options)) {
            foreach ($this->options as $option) {
                $artifact['#' . self::OPTIONS_KEY][$option->getID()] = $option->getTitle();
            }
        }
        return $artifact;
    }
}
