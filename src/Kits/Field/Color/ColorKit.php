<?php

namespace Drupal\formfactorykits\Kits\Field\Color;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class ColorKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;

    public static ?string $id = 'color';
    public static ?string $type = 'color';
}
