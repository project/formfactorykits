<?php

namespace Drupal\formfactorykits\Kits\Field;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\formfactorykits\Kits\Traits\PrefixTrait;
use Drupal\formfactorykits\Kits\Traits\SuffixTrait;
use Drupal\formfactorykits\Kits\Traits\ValueTrait;

abstract class FieldKit extends FormFactoryKit {
    use ValueTrait;
    use PrefixTrait;
    use SuffixTrait;
}
