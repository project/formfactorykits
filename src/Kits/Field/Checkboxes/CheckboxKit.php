<?php

namespace Drupal\formfactorykits\Kits\Field\Checkboxes;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class CheckboxKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;
    use RequiredTrait;

    public static ?string $id = 'checkbox';
    public static ?string $type = 'checkbox';
}
