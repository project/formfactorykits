<?php

namespace Drupal\formfactorykits\Kits\Field\Checkboxes;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\OptionsTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class CheckboxesKit extends FieldKit {
    use DescriptionTrait;
    use OptionsTrait;
    use TitleTrait;
    use RequiredTrait;

    public const OPTIONS_KEY = 'options';

    public static ?string $id = 'checkboxes';
    public static ?string $type = 'checkboxes';

    public function getArray(): array
    {
        $artifact = parent::getArray();
        if (isset($this->options)) {
            foreach ($this->options as $option) {
                $artifact['#' . self::OPTIONS_KEY][$option->getID()] = $option->getTitle();
            }
        }
        return $artifact;
    }
}
