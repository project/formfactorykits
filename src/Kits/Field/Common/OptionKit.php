<?php

namespace Drupal\formfactorykits\Kits\Field\Common;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class OptionKit extends FieldKit {
    use TitleTrait;
}
