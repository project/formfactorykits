<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class TextFieldKit extends FieldKit {
    use DescriptionTrait;
    use SizeTrait;
    use TitleTrait;
    use RequiredTrait;

    public const MAXLENGTH_KEY = 'maxlength';
    public const SIZE_KEY = 'size';
    public const AUTOCOMPLETE_ROUTE_NAME_KEY = 'autocomplete_route_name';
    public const AUTOCOMPLETE_ROUTE_PARAMETERS_KEY = 'autocomplete_route_parameters';

    public static ?string $id = 'textfield';
    public static ?string $type = 'textfield';

    public function setMaxLength(int $length): static
    {
        return $this->set(self::MAXLENGTH_KEY, $length);
    }

    public function setAutoCompleteRoute(string $route): static
    {
        return $this->set(self::AUTOCOMPLETE_ROUTE_NAME_KEY, $route);
    }

    public function getAutoCompleteRouteParameters(array $default = []): array
    {
        return $this->get(self::AUTOCOMPLETE_ROUTE_PARAMETERS_KEY, $default);
    }

    public function setAutoCompleteRouteParameters(array $params): static
    {
        return $this->set(self::AUTOCOMPLETE_ROUTE_PARAMETERS_KEY, $params);
    }

    public function setAutoCompleteRouteParameter(string $key, string $value): static
    {
        $params = $this->getAutoCompleteRouteParameters();
        $params[$key] = $value;
        return $this->setAutoCompleteRouteParameters($params);
    }
}
