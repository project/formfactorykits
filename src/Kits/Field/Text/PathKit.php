<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class PathKit extends TextFieldKit {
    use DescriptionTrait;
    use TitleTrait;

    public const CONVERT_PATH_KEY = 'convert_path';
    public const VALIDATE_PATH_KEY = 'validate_path';

    public static ?string $id = 'path';
    public static ?string $type = 'path';
    public static ?string $title = 'Path';

    /**
     * Sets the field to convert the submitted value to the given setting.
     *
     * @see PathElement::CONVERT_ROUTE
     * @see PathElement::CONVERT_URL
     * @see PathElement::CONVERT_NONE
     */
    public function setConversion(int $setting): static
    {
        return $this->set(self::CONVERT_PATH_KEY, $setting);
    }

    public function setValidate(bool $validate = TRUE): static
    {
        return $this->set(self::VALIDATE_PATH_KEY, $validate);
    }
}
