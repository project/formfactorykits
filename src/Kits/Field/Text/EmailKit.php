<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DefaultValueTrait;
use Drupal\formfactorykits\Kits\Traits\PatternTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;

class EmailKit extends FieldKit {
    use DefaultValueTrait;
    use PatternTrait;
    use SizeTrait;

    public const DEFAULT_VALUE_KEY = 'default_value';
    public const SIZE_KEY = 'size';
    public const PATTERN_KEY = 'pattern';

    public static ?string $id = 'email';
    public static ?string $type = 'email';
}
