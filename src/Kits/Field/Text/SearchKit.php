<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class SearchKit extends FieldKit {
    use TitleTrait;

    public static ?string $id = 'search';
    public static ?string $type = 'search';
}
