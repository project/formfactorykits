<?php

namespace Drupal\formfactorykits\Kits\Field\Text\Number;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DefaultValueTrait;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class RangeKit extends FieldKit {
    use DefaultValueTrait;
    use DescriptionTrait;
    use TitleTrait;

    public const MINIMUM_KEY = 'min';
    public const MAXIMUM_KEY = 'max';
    public const STEP_KEY = 'step';

    public static ?string $id = 'range';
    public static ?string $type = 'range';
    public static ?string $title = 'Range';

    public function setMinimum(int $min): static
    {
        return $this->set(self::MINIMUM_KEY, $min);
    }

    public function setMaximum(int $max): static
    {
        return $this->set(self::MAXIMUM_KEY, $max);
    }

    public function setStep(int $step): static
    {
        return $this->set(self::STEP_KEY, $step);
    }
}
