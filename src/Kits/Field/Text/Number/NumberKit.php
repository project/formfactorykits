<?php

namespace Drupal\formfactorykits\Kits\Field\Text\Number;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DefaultValueTrait;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class NumberKit extends FieldKit {
    use DefaultValueTrait;
    use TitleTrait;
    use DescriptionTrait;

    public const MINIMUM_KEY = 'min';
    public const MAXIMUM_KEY = 'max';
    public const STEP_KEY = 'step';
    public const SIZE_KEY = 'size';

    public static ?string $id = 'number';
    public static ?string $type = 'number';

    public function setMinimum(int $min): static
    {
        return $this->set(self::MINIMUM_KEY, $min);
    }

    public function setMaximum(int $max): static
    {
        return $this->set(self::MAXIMUM_KEY, $max);
    }

    public function setStep(int $step): static
    {
        return $this->set(self::STEP_KEY, $step);
    }

    public function setSize(int $size): static
    {
        return $this->set(self::SIZE_KEY, $size);
    }
}
