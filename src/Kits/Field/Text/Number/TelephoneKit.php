<?php

namespace Drupal\formfactorykits\Kits\Field\Text\Number;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\PatternTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class TelephoneKit extends FieldKit {
    use DescriptionTrait;
    use PatternTrait;
    use SizeTrait;
    use TitleTrait;

    public const PATTERN_KEY = 'pattern';
    public const SIZE_KEY = 'size';

    public static ?string $id = 'telephone';
    public static ?string $type = 'tel';
    public static ?string $title = 'Phone';
}
