<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DefaultValueTrait;
use Drupal\formfactorykits\Kits\Traits\PatternTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;

class PasswordConfirmKit extends FieldKit {
    use DefaultValueTrait;
    use SizeTrait;
    use PatternTrait;

    public const SIZE_KEY = 'size';
    public const PATTERN_KEY = 'pattern';

    public static ?string $id = 'password_confirm';
    public static ?string $type = 'password_confirm';
}
