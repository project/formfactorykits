<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\PatternTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;

class UrlKit extends FieldKit {
    use DescriptionTrait;
    use PatternTrait;
    use SizeTrait;

    public const DEFAULT_VALUE_KEY = 'default_value';
    public const SIZE_KEY = 'size';
    public const PATTERN_KEY = 'pattern';

    public static ?string $id = 'url';
    public static ?string $type = 'url';
    public static ?string $title = 'URL';
}
