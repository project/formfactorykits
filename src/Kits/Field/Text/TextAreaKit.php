<?php

namespace Drupal\formfactorykits\Kits\Field\Text;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class TextAreaKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;
    use RequiredTrait;

    public static ?string $id = 'textarea';
    public static ?string $type = 'textarea';
}
