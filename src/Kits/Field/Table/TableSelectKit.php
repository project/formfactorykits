<?php

namespace Drupal\formfactorykits\Kits\Field\Table;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\StickyTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;
use Drupal\kits\Services\KitsInterface;

class TableSelectKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;
    use StickyTrait;

    public const HEADER_KEY = 'header';
    public const OPTIONS_KEY = 'options';
    public const EMPTY_KEY = 'empty';
    public const JS_SELECT_KEY = 'js_select';
    public const MULTIPLE_KEY = 'multiple';

    public static ?string $id = 'tableselect';
    public static ?string $type = 'tableselect';

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::OPTIONS_KEY, $parameters)) {
            $parameters[self::OPTIONS_KEY] = [];
        }
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->setEmptyMessage(new TranslatableMarkup('No options available.'));
    }

    public function appendHeaderColumn(string $column, string $title): static
    {
        $header = $this->getHeader();
        $header[$column] = $title;
        $this->setHeader($header);
        return $this;
    }

    public function getHeader(array $default = []): array
    {
        return $this->get(self::HEADER_KEY, $default);
    }

    public function setHeader(array $columns): static
    {
        return $this->set(self::HEADER_KEY, $columns);
    }

    public function appendOption(string $option, array $row): static
    {
        $this->parameters['options'][$option] = $row;
        return $this;
    }

    public function setOptions(array $options = []): static
    {
        $this->parameters['options'] = $options;
        return $this;
    }

    public function setMultiple(bool $state = TRUE): static
    {
        return $this->set(self::MULTIPLE_KEY, $state);
    }

    public function setSingle(): static
    {
        return $this->setMultiple(FALSE);
    }

    public function setEmptyMessage(TranslatableMarkup $message): static
    {
        return $this->set(self::EMPTY_KEY, $message);
    }

}
