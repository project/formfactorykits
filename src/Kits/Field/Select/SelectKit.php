<?php

namespace Drupal\formfactorykits\Kits\Field\Select;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\OptionsTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;
use Drupal\kits\Services\KitsInterface;

class SelectKit extends FieldKit {
    use DescriptionTrait;
    use OptionsTrait;
    use TitleTrait;

    public const OPTIONS_KEY = 'options';
    public const EMPTY_OPTION_KEY = 'empty_option';
    public const EMPTY_VALUE_KEY = 'empty_value';
    public const MULTIPLE_KEY = 'multiple';

    public static ?string $id = 'select';
    public static ?string $type = 'select';

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::EMPTY_OPTION_KEY, $parameters)) {
            $parameters[self::EMPTY_OPTION_KEY] = '';
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function setMultiple(bool $isMultiple = TRUE): static
    {
        return $this->set(self::MULTIPLE_KEY, $isMultiple);
    }

    public function isMultiple(): bool
    {
        return (bool)$this->get(self::MULTIPLE_KEY);
    }

    public function getArray(): array
    {
        $artifact = parent::getArray();
        if (isset($this->options)) {
            foreach ($this->options as $option) {
                $artifact['#' . self::OPTIONS_KEY][$option->getID()] = $option->getTitle();
            }
        }
        return $artifact;
    }
}
