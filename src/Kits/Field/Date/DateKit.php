<?php

namespace Drupal\formfactorykits\Kits\Field\Date;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class DateKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;
    use RequiredTrait;

    public static ?string $id = 'date';
    public static ?string $type = 'date';
}
