<?php

namespace Drupal\formfactorykits\Kits\Field\Date;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class DateListKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;

    public const DATE_INCREMENT_KEY = 'date_increment';
    public const DATE_PART_ORDER_KEY = 'date_part_order';

    public static ?string $id = 'datelist';
    public static ?string $type = 'datelist';

    public function setIncrement(string $increment): static
    {
        return $this->set(self::DATE_INCREMENT_KEY, $increment);
    }

    public function setDatePartOrder(string $order): static
    {
        return $this->set(self::DATE_PART_ORDER_KEY, $order);
    }
}
