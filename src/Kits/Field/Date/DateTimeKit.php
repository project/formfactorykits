<?php

namespace Drupal\formfactorykits\Kits\Field\Date;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class DateTimeKit extends FieldKit {
    use DescriptionTrait;
    use TitleTrait;

    public const DATE_INCREMENT_KEY = 'date_increment';
    public const DATE_TIMEZONE_KEY = 'date_timezone';
    public static ?string $id = 'datetime';
    public static ?string $type = 'datetime';

    public function setIncrement(string $increment): static
    {
        return $this->set(self::DATE_INCREMENT_KEY, $increment);
    }

    public function setTimeZone(\DateTimeZone $timezone): static
    {
        return $this->set(self::DATE_TIMEZONE_KEY, $timezone->getName());
    }
}
