<?php

namespace Drupal\formfactorykits\Kits\Field;

class HiddenKit extends FieldKit {
    public static ?string $id = 'hidden';
    public static ?string $type = 'hidden';
}
