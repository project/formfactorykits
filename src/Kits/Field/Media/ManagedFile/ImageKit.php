<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile;

use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\FileSizeUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageExtensionUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageResolutionUploadValidatorKit;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\ThemeTrait;
use Drupal\kits\Services\KitsInterface;

class ImageKit extends FileKit {
    use ThemeTrait;

    public static ?string $id = 'managed_image';

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->setTheme('image_widget');
        $this->setPreviewImageStyle('medium');
        $this->setUploadValidator(FileSizeUploadValidatorKit::create($kitsService));
        $this->setUploadValidator(ImageExtensionUploadValidatorKit::create($kitsService));
    }

    public function setValidExtensions(array $extensions): static
    {
        return $this->setUploadValidator(ImageExtensionUploadValidatorKit::create($this->kitsService)
            ->setExtensions($extensions));
    }

    public function setMaxResolution(int $width, int $height): static
    {
        return $this->setUploadValidator(ImageResolutionUploadValidatorKit::create($this->kitsService)
            ->setMaxWidth($width)
            ->setMaxHeight($height));
    }

    public function setPreviewImageStyle(string $style): static
    {
        return $this->set(self::PREVIEW_IMAGE_STYLE_KEY, $style);
    }
}
