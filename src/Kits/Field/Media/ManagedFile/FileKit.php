<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\FileSizeUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\UploadValidatorKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\MultipleTrait;
use Drupal\formfactorykits\Kits\Traits\RequiredTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class FileKit extends FieldKit {
    use DescriptionTrait;
    use MultipleTrait;
    use TitleTrait;
    use RequiredTrait;

    public const THEME_KEY = 'theme';
    public const MULTIPLE_KEY = 'multiple';
    public const PREVIEW_IMAGE_STYLE_KEY = 'preview_image_style';
    public const UPLOAD_LOCATION_KEY = 'upload_location';
    public const UPLOAD_VALIDATORS_KEY = 'upload_validators';

    public static ?string $id = 'managed_file';
    public static ?string $type = 'managed_file';

    public function setUploadLocation(string $location): static
    {
        return $this->set(self::UPLOAD_LOCATION_KEY, $location);
    }

    public function getUploadValidators(): array
    {
        return $this->get(self::UPLOAD_VALIDATORS_KEY, []);
    }

    private function setUploadValidators(array $validators): static
    {
        return $this->set(self::UPLOAD_VALIDATORS_KEY, $validators);
    }

    public function setUploadValidator(UploadValidatorKit $validator): static
    {
        $validators = $this->getUploadValidators();
        $validators[$validator::CALLBACK] = $validator;
        return $this->setUploadValidators($validators);
    }

    private function getFileSizeValidator(): ?FileSizeUploadValidatorKit
    {
        foreach ($this->getUploadValidators() as $validator) {
            if ($validator instanceof FileSizeUploadValidatorKit) {
                return $validator;
            }
        }
        return NULL;
    }

    public function setMaxFileSize(int $bytes): static
    {
        return $this->setUploadValidator(FileSizeUploadValidatorKit::create($this->kitsService)
            ->setFileSize($bytes));
    }

    public function getMaxFileSizeBytes(): int
    {
        $fileSizeValidator = $this->getFileSizeValidator();
        if (!$fileSizeValidator) {
            return 0;
        }
        return $fileSizeValidator->getFileSize();
    }

    public function getMaxFileSize(): ?string
    {
        return format_size($this->getMaxFileSizeBytes());
    }

    public function getArray(): array
    {
        $array = parent::getArray();
        if (isset($array['#upload_validators'])) {
            foreach ($array['#upload_validators'] as $callback => $uploadValidator) {
                /** @var UploadValidatorKit $uploadValidator */
                $array['#upload_validators'][$callback] = $uploadValidator->getArray();
            }
        }
        return $array;
    }
}
