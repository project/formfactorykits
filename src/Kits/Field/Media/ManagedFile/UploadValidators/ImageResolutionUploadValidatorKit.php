<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators;

class ImageResolutionUploadValidatorKit extends UploadValidatorKit {
    public const CALLBACK = 'file_validate_image_resolution';
    public const MAX_WIDTH_KEY = 'max_width';
    public const MAX_HEIGHT_KEY = 'max_height';

    public function getCallbackArguments(): array
    {
        return [
            $this->getMaxResolution(),
        ];
    }

    public function getMaxResolution(): string
    {
        return sprintf('%dx%d', $this->getMaxWidth(), $this->getMaxHeight());
    }

    public function getMaxWidth(int $default = 0): int
    {
        return $this->getContext(self::MAX_WIDTH_KEY, $default);
    }

    public function setMaxWidth(int $maxWidth): static
    {
        return $this->setContext(self::MAX_WIDTH_KEY, (int)$maxWidth);
    }

    public function getMaxHeight(int $default = 0): int
    {
        return $this->getContext(self::MAX_HEIGHT_KEY, $default);
    }

    public function setMaxHeight(int $maxHeight): static
    {
        return $this->setContext(self::MAX_HEIGHT_KEY, (int)$maxHeight);
    }
}
