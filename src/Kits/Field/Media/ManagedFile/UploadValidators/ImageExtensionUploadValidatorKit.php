<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators;

use Drupal\kits\Services\KitsInterface;

class ImageExtensionUploadValidatorKit extends UploadValidatorKit {
    public const CALLBACK = 'file_validate_extensions';
    public const EXTENSIONS_KEY = 'extensions';
    public const EXTENSIONS = [
        'png',
        'gif',
        'jpg',
        'jpeg',
    ];

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::EXTENSIONS_KEY, $context) && NULL !== static::EXTENSIONS) {
            $context[self::EXTENSIONS_KEY] = static::EXTENSIONS;
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function getCallbackArguments(): array
    {
        return [
            implode(' ', $this->getExtensions()),
        ];
    }

    public function getExtensions(array $default = []): array
    {
        return $this->getContext(self::EXTENSIONS_KEY, $default);
    }

    public function setExtensions(array $extensions = []): static
    {
        return $this->setContext(self::EXTENSIONS_KEY, $extensions);
    }

    public function appendExtension($ext): static
    {
        $extensions = $this->getExtensions();
        $extensions[] = $ext;
        return $this->setExtensions($extensions);
    }
}
