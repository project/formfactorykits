<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\kits\Services\KitsInterface;

class UploadValidatorKit extends FieldKit {
    public const CALLBACK_KEY = 'callback';
    public const CALLBACK = NULL;
    public const CALLBACK_ARGUMENTS_KEY = 'callback_arguments';
    public const CALLBACK_ARGUMENTS = [];

    public function getCallbackArguments(): array
    {
        return $this->getContext(self::CALLBACK_ARGUMENTS_KEY, []);
    }

    public function setCallbackArguments(array $args = []): static
    {
        return $this->setContext(self::CALLBACK_ARGUMENTS_KEY, $args);
    }

    public function getArray(): array
    {
        return $this->getCallbackArguments();
    }
}
