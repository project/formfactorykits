<?php

namespace Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators;

use Drupal\kits\Services\KitsInterface;

class FileSizeUploadValidatorKit extends UploadValidatorKit {
    public const CALLBACK = 'file_validate_size';
    public const FILE_SIZE_KEY = 'file_size';
    public const FILE_SIZE_DEFAULT = 5242880;

    public function __construct(KitsInterface $kitsService, $id = NULL, array $parameters = [], array $context = [])
    {
        if (!array_key_exists(self::FILE_SIZE_KEY, $context)) {
            $context[self::FILE_SIZE_KEY] = self::FILE_SIZE_DEFAULT;
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function getCallbackArguments(): array
    {
        return [
            $this->getContext(self::FILE_SIZE_KEY),
        ];
    }

    public function setFileSize(int $size): static
    {
        return $this->setContext(self::FILE_SIZE_KEY, $size);
    }

    public function getFileSize(): int
    {
        return $this->getContext(self::FILE_SIZE_KEY);
    }
}
