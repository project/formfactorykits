<?php

namespace Drupal\formfactorykits\Kits\Field\Media;

use Drupal\formfactorykits\Kits\Field\FieldKit;
use Drupal\formfactorykits\Kits\Traits\DescriptionTrait;
use Drupal\formfactorykits\Kits\Traits\MultipleTrait;
use Drupal\formfactorykits\Kits\Traits\SizeTrait;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class FileKit extends FieldKit {
    use DescriptionTrait;
    use MultipleTrait;
    use SizeTrait;
    use TitleTrait;

    public const MULTIPLE_KEY = 'multiple';
    public const SIZE_KEY = 'size';

    public static ?string $id = 'file';
    public static ?string $type = 'file';
}
