<?php

namespace Drupal\formfactorykits\Kits;

use Drupal\kits\KitInterface;

interface FormFactoryKitInterface extends KitInterface {
}
