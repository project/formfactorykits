<?php

namespace Drupal\formfactorykits\Kits\Button;

use Drupal\kits\Services\KitsInterface;

class SubmitButtonKit extends ButtonKit {
    public static ?string $id = 'submit';
    public static ?string $type = 'submit';
    public static ?string $value = 'Submit';

    public function __construct(KitsInterface $kitsService, $id = NULL, array $parameters = [], array $context = [])
    {
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->setButtonType('primary');
    }

}
