<?php

namespace Drupal\formfactorykits\Kits\Button;

use Drupal\formfactorykits\Kits\Traits\AttributesTrait;

class ImageButtonKit extends ButtonKit {

    use AttributesTrait;

    public static ?string $id = 'image_button';

    public static ?string $type = 'image_button';

    public const SOURCE_KEY = 'src';

    public function setAlternativeText(string $title): static
    {
        return $this->setAttribute('alt', $title);
    }

    public function setSource(string $source): static
    {
        return $this->set(self::SOURCE_KEY, $source);
    }
}
