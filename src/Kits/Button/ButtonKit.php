<?php

namespace Drupal\formfactorykits\Kits\Button;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\formfactorykits\Kits\Traits\AjaxTrait;
use Drupal\formfactorykits\Kits\Traits\PrefixTrait;
use Drupal\formfactorykits\Kits\Traits\SuffixTrait;
use Drupal\formfactorykits\Kits\Traits\ValueTrait;
use Drupal\kits\Services\KitsInterface;

class ButtonKit extends FormFactoryKit {

    use AjaxTrait;
    use ValueTrait;
    use PrefixTrait;
    use SuffixTrait;

    public const BUTTON_TYPE_KEY = 'button_type';

    public static ?string $id = 'button';

    public static ?string $type = 'button';

    public static ?string $value = NULL;

    public function __construct(KitsInterface $kitsService,
                                              $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!isset($parameters[self::VALUE_KEY]) && !empty(static::$value)) {
            $parameters[self::VALUE_KEY] = $kitsService->t(static::$value);
        }
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->set('name', $id);
    }

    public function setButtonType(string $type): static
    {
        return $this->set(self::BUTTON_TYPE_KEY, $type);
    }
}
