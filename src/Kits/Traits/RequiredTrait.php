<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait RequiredTrait {
    public function setRequired(bool $required = TRUE): static
    {
        return $this->set(self::REQUIRED_KEY, $required);
    }
}
