<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait ThemeTrait {
    public function setTheme(string $theme): static
    {
        return $this->set(self::THEME_KEY, $theme);
    }
}
