<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait WrapTrait {
    use PrefixTrait;
    use SuffixTrait;

    public function wrap(string $id = NULL): static {
        if ($id === NULL) {
            $id = $this->getID() . '-wrapper';
        }
        $this->setPrefix('<div id="' . $id . '">');
        $this->setSuffix('</div>');
        return $this;
    }
}
