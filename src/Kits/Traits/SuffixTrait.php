<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait SuffixTrait {
    public function setSuffix(string|array|null $value): static
    {
        return $this->set('suffix', $value);
    }
}
