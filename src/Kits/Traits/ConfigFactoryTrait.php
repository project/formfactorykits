<?php

namespace Drupal\formfactorykits\Kits\Traits;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

trait ConfigFactoryTrait {

    public function getConfigFactoryService(): ConfigFactoryInterface
    {
        /** @var ConfigFactoryInterface $service */
        static $service;
        if (NULL === $service) {
            $service = $this->kitsService->getContainer()
                ->get('config.factory');
        }
        return $service;
    }

    public function getConfig(string $name): ImmutableConfig
    {
        return $this->getConfigFactoryService()->get($name);
    }

    public function getConfigData(string $name): array
    {
        $config = $this->getConfig($name);
        if (!$config) {
            return [];
        }
        return $config->getRawData();
    }
}
