<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait TitleTrait {
    public function getTitle(?string $default = NULL): ?string
    {
        return $this->get(self::TITLE_KEY, $default);
    }

    public function setTitle(string $title): static
    {
        return $this->set(self::TITLE_KEY, $title);
    }
}
