<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait DefaultValueTrait {

    public function getDefaultValue(array|string|null $default = NULL): array|string|null
    {
        return $this->get(static::VALUE_DEFAULT_KEY, $default);
    }

    public function setDefaultValue(array|string|null $value): static
    {
        return $this->set(static::VALUE_DEFAULT_KEY, $value);
    }
}
