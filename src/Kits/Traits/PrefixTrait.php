<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait PrefixTrait {
    public function setPrefix(string|array|null $value): static
    {
        return $this->set('prefix', $value);
    }
}
