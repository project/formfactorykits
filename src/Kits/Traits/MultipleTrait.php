<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait MultipleTrait {
    public function setMultiple(bool $isMultiple = TRUE): static
    {
        return $this->set(self::MULTIPLE_KEY, $isMultiple);
    }
}
