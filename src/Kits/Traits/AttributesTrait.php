<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait AttributesTrait {

    public function getAttributes(): array
    {
        return $this->get(self::ATTRIBUTES_KEY, []);
    }

    public function setAttributes(array $attributes): static
    {
        return $this->set(self::ATTRIBUTES_KEY, $attributes);
    }

    public function getAttribute(string $name): array|string|null
    {
        $attributes = $this->getAttributes();
        return $attributes[$name] ?? NULL;
    }

    public function setAttribute(string $name, array|string|null $value): static
    {
        $attributes = $this->getAttributes();
        $attributes[$name] = $value;
        return $this->setAttributes($attributes);
    }
}
