<?php

namespace Drupal\formfactorykits\Kits\Traits;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\formfactorykits\Kits\Field\Common\OptionKit;

trait OptionsTrait {
    use StringTranslationTrait;

    /**
     * @var OptionKit[]
     */
    public array $options;

    public function setOptions(array $options): static
    {
        foreach ($options as $key => $option) {
            if (!$option instanceof OptionKit) {
                $options[$key] = OptionKit::create($this->kitsService)
                    ->setID($key)
                    ->setTitle($option);
            }
        }
        $this->options = $options;
        return $this;
    }

    public function appendOption(OptionKit|array $mixed): static
    {
        if ($mixed instanceof OptionKit) {
            $this->options[] = $mixed;
        } elseif (is_array($mixed)) {
            $this->options[] = OptionKit::create($this->kitsService)
                ->setID(key($mixed))
                ->setTitle($mixed[key($mixed)]);
        } elseif (is_string($mixed)) {
            $this->options[] = OptionKit::create($this->kitsService)
                ->setID($mixed)
                ->setTitle($this->t($mixed));
        }
        return $this;
    }
}
