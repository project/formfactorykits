<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait SizeTrait {
    public function setSize(int $size): static
    {
        return $this->set(self::SIZE_KEY, $size);
    }
}
