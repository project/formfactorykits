<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait DescriptionTrait {

    public function getDescription(string|null $default = NULL): string
    {
        return $this->get(static::DESCRIPTION_KEY, $default);
    }

    public function setDescription(string $description): static
    {
        return $this->set(static::DESCRIPTION_KEY, $description);
    }
}
