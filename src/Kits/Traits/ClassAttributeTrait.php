<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait ClassAttributeTrait {
    use AttributesTrait;

    public function appendClass(string $class): static
    {
        $classes = $this->getClasses();
        $classes[] = $class;
        return $this->setAttribute('class', $classes);
    }

    public function getClasses(): array
    {
        return $this->getAttribute('class');
    }

    public function clearClasses(): static
    {
        return $this->setAttribute('class', []);
    }
}
