<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait StickyTrait {
    public function setSticky(): static
    {
        return $this->set('sticky', TRUE);
    }
}
