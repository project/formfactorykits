<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait AjaxTrait {

    public function getAjaxSettings(array $default = []): array
    {
        return $this->get('ajax', $default);
    }

    public function setAjaxSettings(array $array): static
    {
        return $this->set('ajax', $array);
    }

    public function setAjax(string $wrapper = 'ajax-wrapper'): static
    {
        return $this->setAjaxWrapper($wrapper)->setAjaxCallback();
    }

    public function setAjaxSetting(string $key, array|string|null $value): static
    {
        $settings = $this->getAjaxSettings();
        $settings[$key] = $value;
        return $this->setAjaxSettings($settings);
    }

    public function setAjaxCallback(string $callback = '::ajaxFormRebuild'): static
    {
        return $this->setAjaxSetting('callback', $callback);
    }

    public function setAjaxWrapper(string $wrapper = 'ajax-wrapper'): static
    {
        return $this->setAjaxSetting('wrapper', $wrapper);
    }
}
