<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait ValueTrait {
    use DefaultValueTrait;

    public function getValue(string $default = NULL): array|string|null
    {
        return $this->get(static::VALUE_KEY, $default);
    }

    public function setValue(string|array|null $value): static
    {
        return $this->set(static::VALUE_KEY, $value);
    }
}
