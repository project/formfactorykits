<?php

namespace Drupal\formfactorykits\Kits\Traits;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;

trait EntityTrait {

    public function getEntityTypeManagerService(): EntityTypeManagerInterface
    {
        /** @var EntityTypeManagerInterface $service */
        static $service;
        if (NULL === $service) {
            $service = $this->kitsService->getContainer()
                ->get('entity_type.manager');
        }
        return $service;
    }

    public function getEntityQuery(string $entityType, string $conjunction = 'AND'): QueryInterface
    {
        return $this->getEntityTypeManagerService()
            ->getStorage($entityType)
            ->getQuery($conjunction);
    }
}
