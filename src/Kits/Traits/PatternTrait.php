<?php

namespace Drupal\formfactorykits\Kits\Traits;

trait PatternTrait {
    public function setPattern(string $pattern): static
    {
        return $this->set(self::PATTERN_KEY, $pattern);
    }
}
