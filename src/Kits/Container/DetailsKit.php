<?php

namespace Drupal\formfactorykits\Kits\Container;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\formfactorykits\Kits\Traits\TitleTrait;

class DetailsKit extends FormFactoryKit {
    use TitleTrait;

    public const OPEN_KEY = 'open';

    public static ?string $id = 'details';
    public static ?string $type = 'details';

    public function setOpen(bool $isOpen = TRUE): static
    {
        return $this->set(self::OPEN_KEY, $isOpen);
    }
}
