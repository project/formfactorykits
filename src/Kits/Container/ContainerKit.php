<?php

namespace Drupal\formfactorykits\Kits\Container;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\formfactorykits\Kits\Traits\WrapTrait;

class ContainerKit extends FormFactoryKit {
    use WrapTrait;

    protected bool $includeParentsInChildrenArray = FALSE;

    public static ?string $id = 'container';
    public static ?string $type = 'container';
}
