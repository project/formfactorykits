<?php

namespace Drupal\formfactorykits\Kits\Container\Tabs;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\kits\Services\KitsInterface;

class VerticalTabsKit extends FormFactoryKit {
    public const DEFAULT_TAB_KEY = 'default_tab';

    public static ?string $id = 'vertical_tabs';
    public static ?string $type = 'vertical_tabs';

    public function __construct(KitsInterface $kitsService, $id = NULL, array $parameters = [], array $context = [])
    {
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->setChildrenGrouped();
    }

    public function getParents(): array
    {
        return array_merge(parent::getParents(), [$this->getID()]);
    }

    public function getArray(): array
    {
        $artifact = [];
        if (!in_array('parents', $this->excludedParameters)) {
            $parents = $this->getParents();
            if (!empty($parents)) {
                $artifact['#parents'] = $parents;
            }
        }
        foreach ($this->parameters as $parameter => $value) {
            if (NULL !== $value) {
                $artifact['#' . $parameter] = $value;
            }
        }
        return $artifact;
    }

    public function setDefaultTab(string $tab): static
    {
        return $this->set(self::DEFAULT_TAB_KEY, vsprintf('edit-%s', [
            str_replace('_', '-', $tab),
        ]));
    }

    public function createTab(string $id, array $context = []): TabKit
    {
        $kit = TabKit::create(
            kitsService: $this->kitsService,
            id: $id,
            context: $context
        );
        $this->append($kit);
        return $kit;
    }
}
