<?php

namespace Drupal\formfactorykits\Kits\Container\Tabs;

use Drupal\formfactorykits\Kits\Container\DetailsKit;

class TabKit extends DetailsKit {
    public function getArray(): array
    {
        $this->excludeParameter(static::PARENTS_KEY);
        return parent::getArray();
    }

    public function getChildrenArray(): array
    {
        $artifact = [];
        foreach ($this->kits as $kit) {
            /** @var \Drupal\formfactorykits\Kits\FormFactoryKit $kit */
            $kit->excludeParameter($kit::PARENTS_KEY);
            $artifact[$kit->getID()] = $kit->getArray();
        }
        return $artifact;
    }
}
