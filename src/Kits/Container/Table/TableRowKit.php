<?php

namespace Drupal\formfactorykits\Kits\Container\Table;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\kits\Services\KitsInterface;

class TableRowKit extends FormFactoryKit {
    public const ROW_KEY = 'row';

    public function setRow(array|string $row): static
    {
        return $this->set(self::ROW_KEY, $row);
    }

    public function getArray(): array
    {
        return $this->get(self::ROW_KEY);
    }
}
