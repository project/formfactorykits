<?php

namespace Drupal\formfactorykits\Kits\Container\Table;

use Drupal\formfactorykits\Kits\FormFactoryKit;
use Drupal\formfactorykits\Kits\Traits\StickyTrait;

class TableKit extends FormFactoryKit {
    use StickyTrait;

    public const CAPTION_KEY = 'caption';
    public const HEADER_KEY = 'header';
    public const ROWS_KEY = 'rows';
    public const EMPTY_KEY = 'empty';
    public const RESPONSIVE_KEY = 'responsive';

    public static ?string $id = 'table';
    public static ?string $type = 'table';

    /**
     * @var TableRowKit[]
     */
    private array $rows = [];

    public function setCaption(string $caption): static
    {
        return $this->set(self::CAPTION_KEY, $caption);
    }

    public function appendHeaderColumn(string $title): static
    {
        $header = $this->getHeader();
        $header[] = $title;
        $this->setHeader($header);
        return $this;
    }

    public function getHeader(array $default = []): array
    {
        return $this->get(self::HEADER_KEY, $default);
    }

    public function setHeader(array $columns): static
    {
        return $this->set(self::HEADER_KEY, $columns);
    }

    public function createRow(array $row = []): TableRowKit
    {
        return TableRowKit::create($this->kitsService)->setRow($row);
    }

    public function setRows(array $rows = []): static
    {
        foreach ($rows as $key => $row) {
            if (!$row instanceof TableRowKit) {
                $rows[$key] = TableRowKit::create($this->kitsService)->setRow($row);
            }
        }
        $this->rows = $rows;
        return $this;
    }

    public function appendRow(TableRowKit|array $row = []): static
    {
        if (!$row instanceof TableRowKit) {
            $row = $this->createRow($row);
        }
        $this->rows[] = $row;
        return $this;
    }

    public function getArray(): array
    {
        $artifact = parent::getArray();
        foreach ($this->rows as $row) {
            $artifact['#rows'][] = $row->getArray();
        }
        return $artifact;
    }
}
