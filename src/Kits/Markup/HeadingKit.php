<?php

namespace Drupal\formfactorykits\Kits\Markup;

class HeadingKit extends MarkupKit {
    public const NUMBER_KEY = 'number';

    public static ?string $id = 'heading';

    public function getArray(): array
    {
        $artifact = parent::getArray();
        $markup = $this->getMarkup();
        if ($markup) {
            $artifact['#' . self::MARKUP_KEY] = $markup;
        }
        return $artifact;
    }

    public function getMarkup(mixed $default = NULL): ?string
    {
        $value = $this->getValue();
        if (empty($value)) {
            return $default;
        }
        $number = $this->getNumber();
        return vsprintf('<h%d>%s</h%d>', [
            $number,
            $value,
            $number,
        ]);
    }

    public function getValue(string $default = NULL): ?string
    {
        return $this->getContext(self::VALUE_KEY, $default);
    }

    public function setValue(string $value): static
    {
        return $this->setContext(self::VALUE_KEY, $value);
    }

    public function setNumber(int $number): static
    {
        return $this->setContext(static::NUMBER_KEY, $number);
    }

    public function getNumber(int $default = 1): int
    {
        $number = $this->getContext(static::NUMBER_KEY);
        if (NULL === $number) {
            return $default;
        }
        return $number;
    }
}
