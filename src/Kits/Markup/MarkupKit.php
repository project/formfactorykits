<?php

namespace Drupal\formfactorykits\Kits\Markup;

use Drupal\formfactorykits\Kits\FormFactoryKit;

class MarkupKit extends FormFactoryKit {
    public const MARKUP_KEY = 'markup';

    public static ?string $id = 'markup';
    public static ?string $type = 'markup';

    public function getMarkup(string $default = NULL): ?string
    {
        return parent::get(static::MARKUP_KEY, $default);
    }

    public function setMarkup(string $markup): static
    {
        return $this->set(static::MARKUP_KEY, $markup);
    }
}
