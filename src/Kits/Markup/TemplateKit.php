<?php

namespace Drupal\formfactorykits\Kits\Markup;

use Drupal\formfactorykits\Kits\FormFactoryKit;

class TemplateKit extends FormFactoryKit {
  public const TEMPLATE_KEY = 'template';

  public static ?string $id = 'inline_template';
  public static ?string $type = 'inline_template';
  private array $templateContext = [];

  public function getArray(): array
  {
    $array = parent::getArray();
    $array['#context'] = $this->templateContext;
    return $array;
  }

  public function setTemplateFile(string $filename)
  {
    return $this->set(self::TEMPLATE_KEY, file_get_contents($filename));
  }

  public function setTemplate(string $template)
  {
    return $this->set(self::TEMPLATE_KEY, $template);
  }

  public function setTemplateContext(string $key, string $value)
  {
    $this->templateContext[$key] = $value;
    return $this;
  }
}
