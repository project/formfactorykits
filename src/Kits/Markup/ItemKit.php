<?php

namespace Drupal\formfactorykits\Kits\Markup;

use Drupal\formfactorykits\Kits\FormFactoryKit;

class ItemKit extends FormFactoryKit {
    public const MARKUP_KEY = 'markup';

    public static ?string $id = 'item';

    public function getMarkup(): string
    {
        return parent::get(static::MARKUP_KEY);
    }

    public function setMarkup(string $markup): static
    {
        return $this->set(static::MARKUP_KEY, $markup);
    }
}
