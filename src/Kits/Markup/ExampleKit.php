<?php

namespace Drupal\formfactorykits\Kits\Markup;

use Drupal\formfactorykits\Kits\Traits\PrefixTrait;
use Drupal\formfactorykits\Kits\Traits\SuffixTrait;
use Drupal\kits\Services\KitsInterface;

class ExampleKit extends TemplateKit {

    use PrefixTrait;
    use SuffixTrait;

    public function __construct(KitsInterface $kitsService, ?string $id = NULL, array $parameters = [], array $context = []) {
        parent::__construct($kitsService, $id, $parameters, $context);
        $this->setTemplate('<pre>{{ example }}</pre>');
    }

    public function setTitle($title) {
        return $this->setPrefix('<div><strong>' . $title . '</strong></div>');
    }

    public function setExample(string $example) {
        return $this->setTemplateContext('example', $example);
    }

}
