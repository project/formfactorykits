<?php

namespace Drupal\formfactorykits\Kits\Markup;

class TextKit extends MarkupKit {
    public static ?string $id = 'text';

    public function setValue(string $value): static
    {
        return $this->setMarkup(strip_tags($value));
    }
}
