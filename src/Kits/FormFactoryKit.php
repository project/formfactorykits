<?php

namespace Drupal\formfactorykits\Kits;

use Drupal\kits\Kit;
use Drupal\kits\Services\KitsInterface;

abstract class FormFactoryKit extends Kit implements FormFactoryKitInterface {
    public const ARRAY_PARENTS_KEY = 'array_parents';
    public const PARENTS_KEY = 'parents';
    public const TYPE_KEY = 'type';
    public const DESCRIPTION_KEY = 'description';
    public const ATTRIBUTES_KEY = 'attributes';
    public const AFTER_BUILD_KEY = 'after_build';
    public const AJAX_KEY = 'ajax';
    public const DISABLED_KEY = 'disabled';
    public const ELEMENT_VALIDATE_KEY = 'element_validate';
    public const FIELD_PREFIX_KEY = 'field_prefix';
    public const FIELD_SUFFIX_KEY = 'field_suffix';
    public const PROCESS_KEY = 'process';
    public const REQUIRED_KEY = 'required';
    public const STATES_KEY = 'states';
    public const TITLE_KEY = 'title';
    public const TITLE_DISPLAY_KEY = 'title_display';
    public const TREE_KEY = 'tree';
    public const VALUE_KEY = 'value';
    public const VALUE_DEFAULT_KEY = 'default_value';
    public const VALUE_CALLBACK_KEY = 'value_callback';

    public static ?string $type = NULL;
    public static ?string $title = NULL;

    public bool $isChildrenGrouped = FALSE;

    public function __construct(KitsInterface $kitsService,
                                ?string       $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        if (!array_key_exists(self::TITLE_KEY, $parameters) && !empty(static::$title)) {
            $parameters[self::TITLE_KEY] = $kitsService->t(static::$title);
        }
        if (!array_key_exists(self::TYPE_KEY, $parameters) && NULL !== static::$type) {
            $parameters[self::TYPE_KEY] = static::$type;
        }
        parent::__construct($kitsService, $id, $parameters, $context);
    }

    public function getType(): string
    {
        $type = $this->get(self::TYPE_KEY);
        if (empty($type)) {
            throw new \LogicException('Type required');
        }
        return $type;
    }

    public function getArray(): array
    {
        $artifact = [];
        if (!in_array('parents', $this->excludedParameters)) {
            $parents = $this->getParents();
            if (!empty($parents)) {
                $artifact['#parents'] = $parents;
            }
        }
        foreach ($this->parameters as $parameter => $value) {
            if (NULL !== $value) {
                $artifact['#' . $parameter] = $value;
            }
        }
        $artifact += $this->getChildrenArray();
        return $artifact;
    }

    public function setChildrenGrouped(bool $grouped = TRUE): static
    {
        $this->isChildrenGrouped = $grouped;
        return $this;
    }

    public function isChildrenGrouped(): bool
    {
        return $this->isChildrenGrouped;
    }

}
