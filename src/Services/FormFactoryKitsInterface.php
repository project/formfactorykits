<?php

namespace Drupal\formfactorykits\Services;

use Drupal\formfactorykits\Kits\Button\ImageButtonKit;
use Drupal\formfactorykits\Kits\Button\SubmitButtonKit;
use Drupal\formfactorykits\Kits\Container\DetailsKit;
use Drupal\formfactorykits\Kits\Container\Table\TableKit;
use Drupal\formfactorykits\Kits\Container\Table\TableRowKit;
use Drupal\formfactorykits\Kits\Container\Tabs\VerticalTabsKit;
use Drupal\formfactorykits\Kits\Field\Date\DateTimeKit;
use Drupal\formfactorykits\Kits\Field\Entity\NodeAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\Entity\TaxonomyTermAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\Entity\TaxonomyTermSelectKit;
use Drupal\formfactorykits\Kits\Field\Entity\UserAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\HiddenKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\FileKit as ManagedFileKit;
use Drupal\formfactorykits\Kits\Field\Media\FileKit as MediaFileKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\ImageKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\UploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Radios\RadiosKit;
use Drupal\formfactorykits\Kits\Field\Select\SelectKit;
use Drupal\formfactorykits\Kits\Field\Table\TableSelectKit;
use Drupal\formfactorykits\Kits\Field\Text\EmailKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\NumberKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\RangeKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\TelephoneKit;
use Drupal\formfactorykits\Kits\Field\Text\PasswordConfirmKit;
use Drupal\formfactorykits\Kits\Field\Text\PasswordKit;
use Drupal\formfactorykits\Kits\Field\Text\PathKit;
use Drupal\formfactorykits\Kits\Field\Text\SearchKit;
use Drupal\formfactorykits\Kits\Field\Text\TextAreaKit;
use Drupal\formfactorykits\Kits\Field\Text\TextFieldKit;
use Drupal\formfactorykits\Kits\Field\Text\UrlKit;
use Drupal\formfactorykits\Kits\Field\ValueKit;
use Drupal\formfactorykits\Kits\Markup\ExampleKit;
use Drupal\formfactorykits\Kits\Markup\HeadingKit;
use Drupal\formfactorykits\Kits\Markup\ItemKit;
use Drupal\formfactorykits\Kits\Markup\MarkupKit;
use Drupal\formfactorykits\Kits\Markup\TemplateKit;
use Drupal\formfactorykits\Kits\Markup\TextKit;
use Drupal\kits\Services\KitsInterface;
use Drupal\formfactorykits\Kits\Button\ButtonKit;
use Drupal\formfactorykits\Kits\Field\Checkboxes\CheckboxKit;
use Drupal\formfactorykits\Kits\Field\Checkboxes\CheckboxesKit;
use Drupal\formfactorykits\Kits\Field\Color\ColorKit;
use Drupal\formfactorykits\Kits\Container\ContainerKit;
use Drupal\formfactorykits\Kits\Field\Date\DateKit;
use Drupal\formfactorykits\Kits\Field\Date\DateListKit;

interface FormFactoryKitsInterface extends KitsInterface {

    public function button(?string $id = NULL, array $parameters = [], array|string $context = []): ButtonKit;

    public function checkbox(?string $id = NULL, array $parameters = [], array $context = []): CheckboxKit;

    public function checkboxes(?string $id = NULL, array $parameters = [], array $context = []): CheckboxesKit;

    public function color(?string $id = NULL, array $parameters = [], array $context = []): ColorKit;

    public function container(?string $id = NULL, array $parameters = [], array $context = []): ContainerKit;

    public function date(?string $id = NULL, array $parameters = [], array $context = []): DateKit;

    public function dateList(?string $id = NULL, array $parameters = [], array $context = []): DateListKit;

    public function dateTime(?string $id = NULL, array $parameters = [], array $context = []): DateTimeKit;

    public function details(?string $id = NULL, array $parameters = [], array $context = []): DetailsKit;

    public function email(?string $id = NULL, array $parameters = [], array $context = []): EmailKit;

    public function example(?string $id = NULL, array $parameters = [], array $context = []): ExampleKit;

    public function file(?string $id = NULL, array $parameters = [], array $context = []): ManagedFileKit;

    public function fileUnmanaged(?string $id = NULL, array $parameters = [], array $context = []): MediaFileKit;

    public function heading(?string $id = NULL, array $parameters = [], array $context = []): HeadingKit;

    public function hidden(?string $id = NULL, array $parameters = [], array $context = []): HiddenKit;

    public function image(?string $id = NULL, array $parameters = [], array $context = []): ImageKit;

    public function imageButton(?string $id = NULL, array $parameters = [], array $context = []): ImageButtonKit;

    public function item(?string $id = NULL, array $parameters = [], array $context = []): ItemKit;

    public function markup(?string $id = NULL, array $parameters = [], array $context = []): MarkupKit;

    public function nodeAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): NodeAutoCompleteKit;

    public function number(?string $id = NULL, array $parameters = [], array $context = []): NumberKit;

    public function password(?string $id = NULL, array $parameters = [], array $context = []): PasswordKit;

    public function passwordConfirm(?string $id = NULL, array $parameters = [], array $context = []): PasswordConfirmKit;

    public function path(?string $id = NULL, array $parameters = [], array $context = []): PathKit;

    public function radios(?string $id = NULL, array $parameters = [], array $context = []): RadiosKit;

    public function range(?string $id = NULL, array $parameters = [], array $context = []): RangeKit;

    public function search(?string $id = NULL, array $parameters = [], array $context = []): SearchKit;

    public function select(?string $id = NULL, array $parameters = [], array $context = []): SelectKit;

    public function submit(?string $id = NULL, array $parameters = [], array $context = []): SubmitButtonKit;

    public function table(?string $id = NULL, array $parameters = [], array $context = []): TableKit;

    public function tableRow(?string $id = NULL, array $parameters = [], array $context = []): TableRowKit;

    public function tableSelect(?string $id = NULL, array $parameters = [], array $context = []): TableSelectKit;

    public function telephone(?string $id = NULL, array $parameters = [], array $context = []): TelephoneKit;

    public function template(?string $id = NULL, array $parameters = [], array $context = []): TemplateKit;

    public function taxonomyTermSelect(?string $id = NULL, array $parameters = [], array $context = []): TaxonomyTermSelectKit;

    public function taxonomyTermAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): TaxonomyTermAutoCompleteKit;

    public function text(?string $id = NULL, array $parameters = [], array $context = []): TextKit;

    public function textField(?string $id = NULL, array $parameters = [], array $context = []): TextFieldKit;

    public function textArea(?string $id = NULL, array $parameters = [], array $context = []): TextAreaKit;

    public function userAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): UserAutoCompleteKit;

    public function url(?string $id = NULL, array $parameters = [], array $context = []): UrlKit;

    public function value(?string $id = NULL, array $parameters = [], array $context = []): ValueKit;

    public function verticalTabs(?string $id = NULL, array $parameters = [], array $context = []): VerticalTabsKit;

    public function uploadValidator($id = NULL, $parameters = [], $context = []): UploadValidatorKit;

}
