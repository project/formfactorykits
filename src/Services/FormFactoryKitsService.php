<?php

namespace Drupal\formfactorykits\Services;

use Drupal\formfactorykits\Kits;
use Drupal\formfactorykits\Kits\Button\ButtonKit;
use Drupal\formfactorykits\Kits\Button\ImageButtonKit;
use Drupal\formfactorykits\Kits\Button\SubmitButtonKit;
use Drupal\formfactorykits\Kits\Container\ContainerKit;
use Drupal\formfactorykits\Kits\Container\DetailsKit;
use Drupal\formfactorykits\Kits\Container\Table\TableKit;
use Drupal\formfactorykits\Kits\Container\Table\TableRowKit;
use Drupal\formfactorykits\Kits\Container\Tabs\VerticalTabsKit;
use Drupal\formfactorykits\Kits\Field\Checkboxes\CheckboxesKit;
use Drupal\formfactorykits\Kits\Field\Checkboxes\CheckboxKit;
use Drupal\formfactorykits\Kits\Field\Color\ColorKit;
use Drupal\formfactorykits\Kits\Field\Date\DateKit;
use Drupal\formfactorykits\Kits\Field\Date\DateListKit;
use Drupal\formfactorykits\Kits\Field\Date\DateTimeKit;
use Drupal\formfactorykits\Kits\Field\Entity\NodeAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\Entity\TaxonomyTermAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\Entity\TaxonomyTermSelectKit;
use Drupal\formfactorykits\Kits\Field\Entity\UserAutoCompleteKit;
use Drupal\formfactorykits\Kits\Field\HiddenKit;
use Drupal\formfactorykits\Kits\Field\Media\FileKit as MediaFileKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\FileKit as ManagedFileKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\ImageKit;
use Drupal\formfactorykits\Kits\Field\Radios\RadiosKit;
use Drupal\formfactorykits\Kits\Field\Select\SelectKit;
use Drupal\formfactorykits\Kits\Field\Table\TableSelectKit;
use Drupal\formfactorykits\Kits\Field\Text\EmailKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\NumberKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\RangeKit;
use Drupal\formfactorykits\Kits\Field\Text\Number\TelephoneKit;
use Drupal\formfactorykits\Kits\Field\Text\PasswordConfirmKit;
use Drupal\formfactorykits\Kits\Field\Text\PasswordKit;
use Drupal\formfactorykits\Kits\Field\Text\PathKit;
use Drupal\formfactorykits\Kits\Field\Text\SearchKit;
use Drupal\formfactorykits\Kits\Field\Text\TextAreaKit;
use Drupal\formfactorykits\Kits\Field\Text\TextFieldKit;
use Drupal\formfactorykits\Kits\Field\Text\UrlKit;
use Drupal\formfactorykits\Kits\Field\ValueKit;
use Drupal\formfactorykits\Kits\Markup\ExampleKit;
use Drupal\formfactorykits\Kits\Markup\HeadingKit;
use Drupal\formfactorykits\Kits\Markup\ItemKit;
use Drupal\formfactorykits\Kits\Markup\MarkupKit;
use Drupal\formfactorykits\Kits\Markup\TemplateKit;
use Drupal\formfactorykits\Kits\Markup\TextKit;
use Drupal\kits\Services\KitsService;

/**
 * Interface FormFactoryKitsInterface
 *
 * @package Drupal\formfactorykits\Kits
 */
class FormFactoryKitsService extends KitsService implements FormFactoryKitsInterface {
    public function button(?string $id = NULL, array $parameters = [], array|string $context = []): ButtonKit
    {
        return Kits\Button\ButtonKit::create($this, $id, $parameters, $context);
    }

    public function checkbox(?string $id = NULL, array $parameters = [], array $context = []): CheckboxKit
    {
        return Kits\Field\Checkboxes\CheckboxKit::create($this, $id, $parameters, $context);
    }

    public function checkboxes(?string $id = NULL, array $parameters = [], array $context = []): CheckboxesKit
    {
        return Kits\Field\Checkboxes\CheckboxesKit::create($this, $id, $parameters, $context);
    }

    public function color(?string $id = NULL, array $parameters = [], array $context = []): ColorKit
    {
        return Kits\Field\Color\ColorKit::create($this, $id, $parameters, $context);
    }

    public function container(?string $id = NULL, array $parameters = [], array $context = []): ContainerKit
    {
        return Kits\Container\ContainerKit::create($this, $id, $parameters, $context);
    }

    public function date(?string $id = NULL, array $parameters = [], array $context = []): DateKit
    {
        return Kits\Field\Date\DateKit::create($this, $id, $parameters, $context);
    }

    public function dateList(?string $id = NULL, array $parameters = [], array $context = []): DateListKit
    {
        return Kits\Field\Date\DateListKit::create($this, $id, $parameters, $context);
    }

    public function dateTime(?string $id = NULL, array $parameters = [], array $context = []): DateTimeKit
    {
        return Kits\Field\Date\DateTimeKit::create($this, $id, $parameters, $context);
    }

    public function details(?string $id = NULL, array $parameters = [], array $context = []): DetailsKit
    {
        return Kits\Container\DetailsKit::create($this, $id, $parameters, $context);
    }

    public function email(?string $id = NULL, array $parameters = [], array $context = []): EmailKit
    {
        return Kits\Field\Text\EmailKit::create($this, $id, $parameters, $context);
    }

    public function example(?string $id = NULL, array $parameters = [], array $context = []): ExampleKit
    {
        return Kits\Markup\ExampleKit::create($this, $id, $parameters, $context);
    }

    public function file(?string $id = NULL, array $parameters = [], array $context = []): ManagedFileKit
    {
        return Kits\Field\Media\ManagedFile\FileKit::create($this, $id, $parameters, $context);
    }

    public function fileUnmanaged(?string $id = NULL, array $parameters = [], array $context = []): MediaFileKit
    {
        return Kits\Field\Media\FileKit::create($this, $id, $parameters, $context);
    }

    public function heading(?string $id = NULL, array $parameters = [], array $context = []): HeadingKit
    {
        return Kits\Markup\HeadingKit::create($this, $id, $parameters, $context);
    }

    public function hidden(?string $id = NULL, array $parameters = [], array $context = []): HiddenKit
    {
        return Kits\Field\HiddenKit::create($this, $id, $parameters, $context);
    }

    public function image(?string $id = NULL, array $parameters = [], array $context = []): ImageKit
    {
        return Kits\Field\Media\ManagedFile\ImageKit::create($this, $id, $parameters, $context);
    }

    public function imageButton(?string $id = NULL, array $parameters = [], array $context = []): ImageButtonKit
    {
        return Kits\Button\ImageButtonKit::create($this, $id, $parameters, $context);
    }

    public function item(?string $id = NULL, array $parameters = [], array $context = []): ItemKit
    {
        return Kits\Markup\ItemKit::create($this, $id, $parameters, $context);
    }

    public function markup(?string $id = NULL, array $parameters = [], array $context = []): MarkupKit
    {
        return Kits\Markup\MarkupKit::create($this, $id, $parameters, $context);
    }

    public function nodeAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): NodeAutoCompleteKit
    {
        return Kits\Field\Entity\NodeAutoCompleteKit::create($this, $id, $parameters, $context);
    }

    public function number(?string $id = NULL, array $parameters = [], array $context = []): NumberKit
    {
        return Kits\Field\Text\Number\NumberKit::create($this, $id, $parameters, $context);
    }

    public function password(?string $id = NULL, array $parameters = [], array $context = []): PasswordKit
    {
        return Kits\Field\Text\PasswordKit::create($this, $id, $parameters, $context);
    }

    public function passwordConfirm(?string $id = NULL, array $parameters = [], array $context = []): PasswordConfirmKit
    {
        return Kits\Field\Text\PasswordConfirmKit::create($this, $id, $parameters, $context);
    }

    public function path(?string $id = NULL, array $parameters = [], array $context = []): PathKit
    {
        return Kits\Field\Text\PathKit::create($this, $id, $parameters, $context);
    }

    public function radios(?string $id = NULL, array $parameters = [], array $context = []): RadiosKit
    {
        return Kits\Field\Radios\RadiosKit::create($this, $id, $parameters, $context);
    }

    public function range(?string $id = NULL, array $parameters = [], array $context = []): RangeKit
    {
        return Kits\Field\Text\Number\RangeKit::create($this, $id, $parameters, $context);
    }

    public function search(?string $id = NULL, array $parameters = [], array $context = []): SearchKit
    {
        return Kits\Field\Text\SearchKit::create($this, $id, $parameters, $context);
    }

    public function select(?string $id = NULL, array $parameters = [], array $context = []): SelectKit
    {
        return Kits\Field\Select\SelectKit::create($this, $id, $parameters, $context);
    }

    public function submit(?string $id = NULL, array $parameters = [], array $context = []): SubmitButtonKit
    {
        return Kits\Button\SubmitButtonKit::create($this, $id, $parameters, $context);
    }

    public function table(?string $id = NULL, array $parameters = [], array $context = []): TableKit
    {
        return Kits\Container\Table\TableKit::create($this, $id, $parameters, $context);
    }

    public function tableRow(?string $id = NULL, array $parameters = [], array $context = []): TableRowKit
    {
        return Kits\Container\Table\TableRowKit::create($this, $id, $parameters, $context);
    }

    public function tableSelect(?string $id = NULL, array $parameters = [], array $context = []): TableSelectKit
    {
        return Kits\Field\Table\TableSelectKit::create($this, $id, $parameters, $context);
    }

    public function telephone(?string $id = NULL, array $parameters = [], array $context = []): TelephoneKit
    {
        return Kits\Field\Text\Number\TelephoneKit::create($this, $id, $parameters, $context);
    }

    public function template(?string $id = NULL, array $parameters = [], array $context = []): TemplateKit
    {
        return Kits\Markup\TemplateKit::create($this, $id, $parameters, $context);
    }

    public function taxonomyTermSelect(?string $id = NULL, array $parameters = [], array $context = []): TaxonomyTermSelectKit
    {
        return Kits\Field\Entity\TaxonomyTermSelectKit::create($this, $id, $parameters, $context);
    }

    public function taxonomyTermAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): TaxonomyTermAutoCompleteKit
    {
        return Kits\Field\Entity\TaxonomyTermAutoCompleteKit::create($this, $id, $parameters, $context);
    }

    public function text(?string $id = NULL, array $parameters = [], array $context = []): TextKit
    {
        return Kits\Markup\TextKit::create($this, $id, $parameters, $context);
    }

    public function textField(?string $id = NULL, array $parameters = [], array $context = []): TextFieldKit
    {
        return Kits\Field\Text\TextFieldKit::create($this, $id, $parameters, $context);
    }

    public function textArea(?string $id = NULL, array $parameters = [], array $context = []): TextAreaKit
    {
        return Kits\Field\Text\TextAreaKit::create($this, $id, $parameters, $context);
    }

    public function userAutoComplete(?string $id = NULL, array $parameters = [], array $context = []): UserAutoCompleteKit
    {
        return Kits\Field\Entity\UserAutoCompleteKit::create($this, $id, $parameters, $context);
    }

    public function url(?string $id = NULL, array $parameters = [], array $context = []): UrlKit
    {
        return Kits\Field\Text\UrlKit::create($this, $id, $parameters, $context);
    }

    public function value(?string $id = NULL, array $parameters = [], array $context = []): ValueKit
    {
        return Kits\Field\ValueKit::create($this, $id, $parameters, $context);
    }

    public function verticalTabs(?string $id = NULL, array $parameters = [], array $context = []): VerticalTabsKit
    {
        return Kits\Container\Tabs\VerticalTabsKit::create($this, $id, $parameters, $context);
    }

    public function uploadValidator($id = NULL, $parameters = [], $context = []): Kits\Field\Media\ManagedFile\UploadValidators\UploadValidatorKit
    {
        return Kits\Field\Media\ManagedFile\UploadValidators\UploadValidatorKit::create($this, $id, $parameters, $context);
    }
}
