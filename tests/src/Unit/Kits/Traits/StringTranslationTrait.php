<?php

namespace Drupal\Tests\formfactorykits\Unit\Kits\Traits;

use Drupal\Core\DependencyInjection\ContainerNotInitializedException;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\TranslationManager;

trait StringTranslationTrait {
    public function getTranslationManager(): TranslationInterface
    {
        try {
            $manager = \Drupal::service('string_translation');
            $manager->reset();
            return $manager;
        } catch (ContainerNotInitializedException $e) {
            return new TranslationManager(new LanguageDefault([]));
        }
    }

    public function t(string $string): TranslatableMarkup
    {
        return new TranslatableMarkup($string, [], [], $this->getTranslationManager());
    }
}
