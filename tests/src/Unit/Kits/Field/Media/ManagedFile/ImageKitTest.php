<?php

namespace Drupal\Tests\formfactorykits\Unit\Kits\Field\Media\ManagedFile;

use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageExtensionUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageResolutionUploadValidatorKit;
use Drupal\Tests\formfactorykits\Unit\KitTestBase;

/**
 * @coversDefaultClass \Drupal\formfactorykits\Kits\Field\Media\ManagedFile\ImageKit
 * @group kit
 */
class ImageKitTest extends KitTestBase {
    public function testDefaults()
    {
        $image = $this->k->image();
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testCustomID()
    {
        $image = $this->k->image('foo');
        $this->assertEquals('foo', $image->getID());
    }

    public function testTitle()
    {
        $image = $this->k->image()
            ->setTitle('Foo');
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
                '#title' => 'Foo',
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testDescription()
    {
        $image = $this->k->image()
            ->setDescription('Foo');
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
                '#description' => 'Foo',
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testValue()
    {
        $image = $this->k->image()
            ->setValue('foo');
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
                '#value' => 'foo',
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testMultiple()
    {
        $image = $this->k->image()
            ->setMultiple();
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
                '#multiple' => TRUE,
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testUploadLocation()
    {
        $image = $this->k->image()
            ->setUploadLocation('foo');
        $this->assertEquals([
            'managed_image' => [
                '#type' => 'managed_file',
                '#theme' => 'image_widget',
                '#preview_image_style' => 'medium',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
                '#upload_location' => 'foo',
            ],
        ], [
            $image->getID() => $image->getArray(),
        ]);
    }

    public function testUploadValidators()
    {
        $image = $this->k->image()
            ->setValidExtensions(['psd', 'tiff'])
            ->setMaxFileSize(1000)
            ->setMaxResolution(1920, 1080);
        $this->assertEquals(
            expected: [
                'managed_image' => [
                    '#type' => 'managed_file',
                    '#theme' => 'image_widget',
                    '#preview_image_style' => 'medium',
                    '#upload_validators' => [
                        'file_validate_extensions' => ['psd tiff'],
                        'file_validate_image_resolution' => ['1920x1080'],
                        'file_validate_size' => [1000],
                    ],
                ],
            ],
            actual: [
                $image->getID() => $image->getArray(),
            ]
        );
    }
}
