<?php

namespace Drupal\Tests\formfactorykits\Unit\Kits\Field\Media\ManagedFile;

use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\FileSizeUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageExtensionUploadValidatorKit;
use Drupal\formfactorykits\Kits\Field\Media\ManagedFile\UploadValidators\ImageResolutionUploadValidatorKit;
use Drupal\Tests\formfactorykits\Unit\KitTestBase;

/**
 * @coversDefaultClass \Drupal\formfactorykits\Kits\Field\Media\ManagedFile\FileKit
 * @group kit
 */
class FileKitTest extends KitTestBase {
    public function testDefaults()
    {
        $file = $this->k->file();
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    public function testCustomID()
    {
        $file = $this->k->file('foo');
        $this->assertEquals('foo', $file->getID());
    }

    public function testTitle()
    {
        $file = $this->k->file()
            ->setTitle('Foo');
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#title' => 'Foo',
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    public function testDescription()
    {
        $file = $this->k->file()
            ->setDescription('Foo');
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#description' => 'Foo',
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    public function testValue()
    {
        $file = $this->k->file()
            ->setValue('foo');
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#value' => 'foo',
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    public function testMultiple()
    {
        $file = $this->k->file()
            ->setMultiple();
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#multiple' => TRUE,
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    public function testUploadLocation()
    {
        $file = $this->k->file()
            ->setUploadLocation('foo');
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#upload_location' => 'foo',
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }

    /**
     * @current
     */
    public function testUploadValidators()
    {
        $file = $this->k->file()
            ->setUploadValidator(FileSizeUploadValidatorKit::create($this->k))
            ->setUploadValidator(ImageExtensionUploadValidatorKit::create($this->k));
        $this->assertEquals([
            'managed_file' => [
                '#type' => 'managed_file',
                '#upload_validators' => [
                    'file_validate_size' => [5242880],
                    'file_validate_extensions' => ['png gif jpg jpeg'],
                ],
            ],
        ], [
            $file->getID() => $file->getArray(),
        ]);
    }
}
